from src.database import Base
from src.job_titles.models import job_titles

from fastapi_users_db_sqlalchemy import SQLAlchemyBaseUserTable
from sqlalchemy import Table, Column, Integer, String, TIMESTAMP, ForeignKey, Boolean, MetaData


metadata = MetaData()

employees = Table(
    "employees",
    metadata,
    Column("id", Integer, primary_key=True),
    Column("first_name", String, nullable=False),
    Column("second_name", String, nullable=False),
    Column("job_title_id", Integer, ForeignKey(job_titles.c.id)),
    Column("email", String, nullable=False),
    Column("login", String, nullable=False),
    Column("hashed_password", String, nullable=False),
    Column("wage_adjustment", Integer, default=0),
    Column("next_promotion_date", TIMESTAMP),
    Column("is_active", Boolean, default=True, nullable=False),
    Column("is_superuser", Boolean, default=False, nullable=False),
    Column("is_verified", Boolean, default=False, nullable=False)
)


class Employees(SQLAlchemyBaseUserTable[int], Base):
    __tablename__ = 'employees'
    id = Column(Integer, primary_key=True)
    first_name = Column(String, nullable=False)
    second_name = Column( String, nullable=False)
    job_title_id = Column(Integer, ForeignKey(job_titles.c.id))
    email = Column(String, nullable=False)
    login = Column(String, nullable=False)
    hashed_password = Column(String, nullable=False)
    wage_adjustment = Column(Integer, default=0)
    next_promotion_date = Column(TIMESTAMP)
    is_active: bool = Column(Boolean, default=True, nullable=False)
    is_superuser: bool = Column(Boolean, default=False, nullable=False)
    is_verified: bool = Column(Boolean, default=False, nullable=False)
