from src.auth.base_config import auth_backend
from src.auth.schemas import UserRead, UserCreate
from src.auth.base_config import fastapi_users
from src.job_titles.router import router as router_job_titles
from src.employees.router import router as router_employees

from fastapi import FastAPI


app = FastAPI(
    title='Salary App'
)


@app.get("/")
def hello():
    return "Hello world!"


app.include_router(
    fastapi_users.get_auth_router(auth_backend),
    prefix="/auth/jwt",
    tags=["auth"],
)

app.include_router(
    fastapi_users.get_register_router(UserRead, UserCreate),
    prefix="/auth",
    tags=["auth"],
)

app.include_router(router_job_titles)
app.include_router(router_employees)
