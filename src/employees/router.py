from datetime import datetime

from src.database import get_async_session
from src.job_titles.models import job_titles
from src.auth.models import employees
from src.auth.models import Employees
from src.employees.schemas import EmployeesUpdate

from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy import select, update
from fastapi import APIRouter, Depends
from src.auth.base_config import fastapi_users


current_user = fastapi_users.current_user()


router = APIRouter(
    prefix="/working-conditions",
    tags=["Working conditions"]
)


@router.get("/")
async def protected_route(user: Employees = Depends(current_user), session: AsyncSession = Depends(get_async_session)):
    query = select(job_titles).where(job_titles.c.id == user.job_title_id)
    job_title = await session.execute(query)
    job_title = job_title.all()[0]
    response = f"Hello, {user.first_name}, your salary: {job_title[2] + user.wage_adjustment} {job_title[3]}"
    return f"{response}, the increase is not planned yet." if user.next_promotion_date is None \
        else f"{response}, date of the next promotion: {user.next_promotion_date} "


@router.post("/")
async def add_employees_info(employees_info: EmployeesUpdate, user: Employees = Depends(current_user), session: AsyncSession = Depends(get_async_session)):
    employees_info = dict(filter(lambda x: x[1] is not None, employees_info.dict().items()))
    employees_info['next_promotion_date'] = datetime.fromisoformat(str(employees_info['next_promotion_date']).split('.')[0])
    stmt = update(employees).values(**employees_info).where(employees.c.id == user.id)
    await session.execute(stmt)
    await session.commit()
    return {"status": "success"}
