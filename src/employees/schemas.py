from datetime import datetime

from typing import Optional
from pydantic import BaseModel


class EmployeesUpdate(BaseModel):
    wage_adjustment: Optional[int]
    next_promotion_date: Optional[datetime]
    job_title_id: Optional[int]

    class Config:
        orm_mode = True
