import os

from dotenv import load_dotenv


load_dotenv()

DB_USER = os.getenv("DB_USER")
DB_PASSWORD = os.getenv("DB_PASSWORD")
DB_HOST = os.getenv("DB_HOST")
DB_NAME = os.getenv("DB_NAME")
DB_PORT = os.getenv("DB_PORT")
KEY_FOR_ENCODING = os.getenv("KEY_FOR_ENCODING")
KEY_FOR_ENCODING_COOKIE = os.getenv("KEY_FOR_ENCODING_COOKIE")
