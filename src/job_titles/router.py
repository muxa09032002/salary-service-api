from src.database import get_async_session
from src.job_titles.models import job_titles
from src.job_titles.schemas import JobTitles, JobTitlesCreate

from pydantic.types import List
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy import select, insert
from fastapi import APIRouter, Depends


router = APIRouter(
    prefix="/job_titles",
    tags=["Job titles"]
)


@router.get("/", response_model=List[JobTitles])
async def get_all_job_titles(name_title: str, session: AsyncSession = Depends(get_async_session)):
    query = select(job_titles).where(job_titles.c.title.ilike(f'%{name_title}%'))
    result = await session.execute(query)
    return result.all()


@router.post("/")
async def add_job_title(new_job_title: JobTitlesCreate, session: AsyncSession = Depends(get_async_session)):
    stmt = insert(job_titles).values(**new_job_title.dict())
    await session.execute(stmt)
    await session.commit()
    return {"status": "success"}
