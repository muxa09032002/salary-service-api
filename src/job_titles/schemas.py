from pydantic import BaseModel


class JobTitles(BaseModel):
    id: int
    title: str
    salary: int
    currency: str

    class Config:
        orm_mode = True


class JobTitlesCreate(BaseModel):
    title: str
    salary: int
    currency: str

    class Config:
        orm_mode = True
