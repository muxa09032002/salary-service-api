from sqlalchemy import Table, Column, Integer, String, MetaData


metadata = MetaData()

job_titles = Table(
    "job_titles",
    metadata,
    Column("id", Integer, primary_key=True),
    Column("title", String, nullable=False),
    Column("salary", Integer, nullable=False),
    Column("currency", String, nullable=False)
)
