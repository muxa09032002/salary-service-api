# salary-service-api

## Задача:
Реализовать REST-сервис просмотра текущей зарплаты и даты следующего
повышения.

## Требования:
Запрос
данных о зарплате должен выдаваться только при предъявлении валидного токена.

## Используемые технологии:
* FastAPI
* PostgreSQL
* SQLAlchemy

## API
* /auth/jwt/login - Вход пользователя (При входе выдаётся cookie с JWT, которая действует в течении часа)
 ```
curl -X 'POST' \
  'http://127.0.0.1:8000/auth/jwt/login' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/x-www-form-urlencoded' \
  -d 'grant_type=&username=testemail&password=testpassword&scope=&client_id=&client_secret='
```

* /auth/jwt/logout - Выход пользователя
 ```
curl -X 'POST' \
  'http://127.0.0.1:8000/auth/jwt/logout' \
  -H 'accept: application/json' \
  -d ''
```

* /auth/register - Регистрация пользователя
 ```
curl -X 'POST' \
  'http://127.0.0.1:8000/auth/register' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -d '{
  "email": "string",
  "password": "string",
  "is_active": true,
  "is_superuser": false,
  "is_verified": false,
  "first_name": "string",
  "second_name": "string",
  "login": "string"
}'
```

* /job_titles/ - Вывод должностей, в названии которых есть name_title
 ```
curl -X 'GET' \
  'http://127.0.0.1:8000/job_titles/?name_title=value' \
  -H 'accept: application/json'
```

* /job_titles/ - Добавление должности
 ```
curl -X 'POST' \
  'http://127.0.0.1:8000/job_titles/' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -d '{
  "title": "string",
  "salary": 0,
  "currency": "string"
}'
```

* /working-conditions/ - проверка заработной платы и следующей даты повышения (необходимо войти в свой аккаунт и убедиться что cookie не истёк)
 ```
curl -X 'GET' \
  'http://127.0.0.1:8000/working-conditions/' \
  -H 'accept: application/json'
```

* /working-conditions/ - установка должности, следующей даты повышения и доп. оплаты (Для своего пользователя)
 ```
curl -X 'POST' \
  'http://127.0.0.1:8000/working-conditions/' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -d '{
  "wage_adjustment": 5000,
  "next_promotion_date": "2023-06-22T19:25:00.822Z",
  "job_title_id": 23
}'
```

## Запуск сервиса
Необходимо установить PostgreSQL, создать сервер и занести данные в .env
```bash
poetry install
alembic revision --autogenerate -m "Database creation"
alembic upgrade head
uvicorn src.main:app --reload
```
