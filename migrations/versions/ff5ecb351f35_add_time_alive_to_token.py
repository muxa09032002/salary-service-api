"""Add time alive to token

Revision ID: ff5ecb351f35
Revises: 06ea8192507e
Create Date: 2023-06-20 01:08:24.314110

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'ff5ecb351f35'
down_revision = '06ea8192507e'
branch_labels = None
depends_on = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('tokens', sa.Column('time_alive', sa.TIMESTAMP(), nullable=True))
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('tokens', 'time_alive')
    # ### end Alembic commands ###
