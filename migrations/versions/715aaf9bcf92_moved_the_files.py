"""moved the files

Revision ID: 715aaf9bcf92
Revises: 8adab8e2d0ea
Create Date: 2023-06-22 22:36:19.944018

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '715aaf9bcf92'
down_revision = '8adab8e2d0ea'
branch_labels = None
depends_on = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    pass
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    pass
    # ### end Alembic commands ###
